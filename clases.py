class perro:  # nombre de la clase

    especie = 'mamifero'  # atributo clase

    def __init__(self, nombre, edad,color):
        self.nombre = nombre
        self.edad = edad
        self.color = color

        """ 
        :param: declara la variable nombre , edad y color

        """
    def descripcion(self):
        return "{} tiene {} años de edad y es de color {}".format(self.nombre, self.edad, self.color)
        """ 
        :return: retorna los valores de las variables nombre , edad y color
        
        """
    def hablar(self, sonido):
        return " {} dice {}".format(self.nombre, sonido)
        """ 
        :return: retorna los valores de las variables nombre  y sonido
        
        """

class rottweiler(perro):
    def correr(self, velocidad):
        print("{} corre a {} y es de color {}".format(self.nombre, velocidad, self.color))


lassie = perro("lassie", 5, "negro")
print(lassie.descripcion())
print(lassie.hablar(" woof woof"))
roco = rottweiler("roco", 10, "blanco")
roco.correr(50)
print(roco.hablar("que lo que eh"))

